package data

import (
	"sync"
)

type SynchronizedBufferedValueQueue struct {
	values []*Value
	mutex  *sync.Mutex
	cond   *sync.Cond
	done   bool
}

func NewSynchronizedBufferedValueQueue(bufferSize int) *SynchronizedBufferedValueQueue {
	var mutex sync.Mutex
	var values = make([]*Value, 0, bufferSize)

	return &SynchronizedBufferedValueQueue{
		values: values,
		mutex:  &mutex,
		cond:   sync.NewCond(&mutex),
	}
}

func (queue *SynchronizedBufferedValueQueue) PutLast(value *Value) {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	for len(queue.values) >= cap(queue.values) && !queue.done {
		queue.cond.Wait()
	}

	if queue.done {
		return
	}

	var lastIndex = len(queue.values)
	queue.values = queue.values[:lastIndex+1]
	queue.values[lastIndex] = value

	queue.cond.Broadcast()
}

func (queue *SynchronizedBufferedValueQueue) TakeLast() (value *Value) {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	for len(queue.values) <= 0 && !queue.done {
		queue.cond.Wait()
	}

	if len(queue.values) == 0 && queue.done {
		return
	}

	var lastIndex = len(queue.values) - 1
	value = queue.values[lastIndex]
	queue.values = queue.values[:lastIndex]

	queue.cond.Broadcast()

	return
}

func (queue *SynchronizedBufferedValueQueue) Done() {
	queue.done = true
}

func (queue *SynchronizedBufferedValueQueue) GetValues() []*Value {
	return queue.values
}

type SynchronizedBufferedOrderedValueQueue struct {
	*SynchronizedBufferedValueQueue
	compare func(a, b *Value) int
}

func NewSynchronizedBufferedOrderedValueQueue(bufferSize int) *SynchronizedBufferedOrderedValueQueue {
	return &SynchronizedBufferedOrderedValueQueue{
		SynchronizedBufferedValueQueue: NewSynchronizedBufferedValueQueue(bufferSize),
		compare: func(a, b *Value) int {
			var diff = a.Total - b.Total

			if diff < 0 {
				return -1
			} else if diff > 0 {
				return 1
			} else {
				return 0
			}
		},
	}
}

func (queue *SynchronizedBufferedOrderedValueQueue) PutOrdered(value *Value) {
	queue.mutex.Lock()
	defer queue.mutex.Unlock()

	for len(queue.values) >= cap(queue.values) && !queue.done {
		queue.cond.Wait()
	}

	if queue.done {
		return
	}

	var index = len(queue.values)
	queue.values = queue.values[:index+1]

	for index > 0 && queue.compare(queue.values[index-1], value) > 0 {
		queue.values[index] = queue.values[index-1]
		index--
	}

	queue.values[index] = value

	queue.cond.Broadcast()
}
