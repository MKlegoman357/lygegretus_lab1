package main

import (
	"./data"
	"./utils"
	"bufio"
	"fmt"
	"io"
	"math"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	if len(os.Args) == 2 && (os.Args[1] == "--generate") {
		generateData()
	} else {
		createFilteredData()
	}
}

func generateData() {
	var reader = bufio.NewReader(os.Stdin)

	var valueCount = getInt(
		reader,
		"Enter amount of values to generate (must be a positive integer):",
		"Invalid amount!",
		100,
		func(value int) bool {
			return value > 0
		},
	)

	var totalMin, totalMax = getFloatRange(
		reader,
		"Enter desired range of resulting total (ex.: '<100', '>10', '10 100'):",
		"<100",
		-math.MaxFloat32,
		math.MaxFloat32,
	)

	var values = generateValues(valueCount, data.Names, totalMin, totalMax)

	if getBool(reader, "Print values?", false) {
		fmt.Println("Generated values:")
		data.ValueSlice(values).Print()
		fmt.Println()
	}

	fmt.Print("Save to file (enter empty path to not save): ")
	outputPath, _ := getLine(reader)

	if len(outputPath) > 0 {
		if !strings.HasSuffix(outputPath, ".json") {
			outputPath += ".json"
		}

		err := data.WriteValuesToFile(values, outputPath)

		if err != nil {
			fmt.Printf("An error occured while trying to save the file: \n%s\n", err.Error())
			return
		}

		fmt.Printf("Values saved to file successfully to %s.\n", outputPath)
	} else {
		fmt.Println("Skipped saving to file.")
	}
}

func createFilteredData() {
	var reader = bufio.NewReader(os.Stdin)

	fmt.Print("Input file: ")

	inputFilePath, _ := getLine(reader)
	stat, err := os.Stat(inputFilePath)

	if os.IsNotExist(err) {
		fmt.Printf("File '%s' does not exist.\n", inputFilePath)
		return
	} else if stat.IsDir() {
		fmt.Printf("File '%s' is a directory.\n", inputFilePath)
		return
	}

	values, err := data.ReadValuesFromFile(inputFilePath)

	if err != nil {
		fmt.Printf("Error while trying to read the file:\n%s\n", err.Error())
	}

	var processorCount = runtime.NumCPU()
	var workerCount = getInt(
		reader,
		"Enter worker goroutine count:",
		"Invalid amount!",
		processorCount-1,
		func(value int) bool {
			return value > 0
		},
	)

	var bufferSize = getInt(
		reader,
		"Enter input queue buffer size:",
		"Invalid amount!",
		(len(values)+1)/2,
		func(value int) bool {
			return value > 0
		},
	)

	var filterFrom, filterTo = getFloatRange(
		reader,
		"Filter values with total (ex.: '<100', '>10', '10 100'):",
		"<50",
		-math.MaxFloat32,
		math.MaxFloat32,
	)

	var results []*data.Value

	if getBool(reader, "Show console output?", false) {
		results = consumeAndFilterValuesWithConsoleOutput(values, filterFrom, filterTo, workerCount, bufferSize)
	} else {
		results = consumeAndFilterValues(values, filterFrom, filterTo, workerCount, bufferSize)
	}

	fmt.Print("Result file path (leave empty to output to the console): ")
	outputFilePath, _ := getLine(reader)

	if len(outputFilePath) > 0 {
		if !strings.HasSuffix(outputFilePath, ".txt") {
			outputFilePath += ".txt"
		}

		file, err := os.Create(outputFilePath)

		if err != nil {
			fmt.Printf("An error occurred while trying to open the output file:\n%s\n", err.Error())
			return
		}

		_, _ = fmt.Fprintln(file, "Original values:")
		printValuesAsTable(file, values)

		_, _ = fmt.Fprintln(file)
		_, _ = fmt.Fprintln(file, "Filtered values:")
		printValuesAsTable(file, results)

		_ = file.Close()
	} else {
		fmt.Println()
		fmt.Println("Filtered results:")
		printValuesAsTable(os.Stdout, results)
	}
}

func consumeAndFilterValues(values []*data.Value, totalFrom, totalTo float32, workerCount, bufferSize int) []*data.Value {
	var waitGroup sync.WaitGroup

	var inputQueue = data.NewSynchronizedBufferedValueQueue(bufferSize)
	var outputQueue = data.NewSynchronizedBufferedOrderedValueQueue(len(values))

	for i := 0; i < workerCount; i++ {
		waitGroup.Add(1)

		go func() {
			defer waitGroup.Done()

			for {
				var value = inputQueue.TakeLast()

				if value == nil {
					return
				}

				value.Total = float32(value.Count) * value.Value

				if value.Total >= totalFrom && value.Total <= totalTo {
					outputQueue.PutOrdered(value)
				}
			}
		}()
	}

	for _, value := range values {
		inputQueue.PutLast(value)
	}

	inputQueue.Done()
	waitGroup.Wait()

	return outputQueue.GetValues()
}

func consumeAndFilterValuesWithConsoleOutput(values data.ValueSlice, totalFrom, totalTo float32, workerCount, bufferSize int) []*data.Value {
	var waitGroup sync.WaitGroup

	var inputQueue = data.NewSynchronizedBufferedValueQueue(bufferSize)
	var outputQueue = data.NewSynchronizedBufferedOrderedValueQueue(len(values))

	fmt.Println("Look guys, I found some values!")
	values.Print()
	fmt.Println()

	for i := 0; i < workerCount; i++ {
		var index = i
		waitGroup.Add(1)

		go func() {
			defer waitGroup.Done()

			for {
				var value = inputQueue.TakeLast()

				if value == nil {
					fmt.Printf("%d Looks like no more values for me :(\n", index)
					return
				}

				fmt.Printf("%d Look mom, a took a value! %s\n", index, value)

				time.Sleep(500 * time.Millisecond)
				value.Total = float32(value.Count) * value.Value

				if value.Total >= totalFrom && value.Total <= totalTo {
					fmt.Printf("%d I'll keep this one. %s\n", index, value)
					outputQueue.PutOrdered(value)
				} else {
					fmt.Printf("%d I don't need this one. %s\n", index, value)
				}
			}
		}()
	}

	for _, value := range values {
		fmt.Printf("My sons, take the value: %s\n", value)
		inputQueue.PutLast(value)
	}

	fmt.Println("Are you guys done yet?")
	inputQueue.Done()
	waitGroup.Wait()

	fmt.Println()
	fmt.Println("Lets see what you guys kept for yourselves:")
	data.ValueSlice(outputQueue.GetValues()).Print()

	fmt.Println("Everything should be done now!")

	return outputQueue.GetValues()
}

func generateValues(amount int, names []string, totalMin, totalMax float32) []*data.Value {
	values := make([]*data.Value, amount)

	for i := range values {
		var total = totalMin + (totalMax-totalMin)*rand.Float32()
		var count = rand.Intn(10)
		var value float32

		if count == 0 && (totalMin > 0 || totalMax < 0) {
			count = 1
		}

		if count == 0 {
			value = total
		} else {
			value = total / float32(count)
		}

		values[i] = &data.Value{
			Name:  names[rand.Intn(len(names))],
			Count: count,
			Value: value,
			Total: float32(count) * value,
		}
	}

	return values
}

func getLine(reader *bufio.Reader) (line string, err error) {
	line, err = reader.ReadString('\n')

	if err == nil {
		line = line[:len(line)-1]
	}

	return
}

func getInt(reader *bufio.Reader, message, wrongFormatMessage string, defaultValue int, cond func(value int) bool) (value int) {
	for {
		fmt.Printf("%s (%d) ", message, defaultValue)
		stringValue, _ := getLine(reader)

		if len(stringValue) == 0 {
			value = defaultValue
		} else {
			intValue, err := strconv.ParseInt(stringValue, 10, 0)

			if err == nil {
				value = int(intValue)
			} else {
				fmt.Printf("%s %s\n", wrongFormatMessage, err.Error())
			}
		}

		if cond(value) {
			return
		}
	}
}

func getFloatRange(reader *bufio.Reader, message, defaultValue string, clampFrom, clampTo float32) (from, to float32) {
	from, to = clampTo, clampFrom

	for from > to {
		fmt.Printf("%s (%s) ", message, defaultValue)
		var rangeString, _ = getLine(reader)
		var err error = nil

		if len(rangeString) == 0 {
			rangeString = defaultValue
		}

		if rangeString[0] == '<' {
			_, err = fmt.Fscanf(strings.NewReader(rangeString[1:]), "%f", &to)

			if err == nil {
				from = clampFrom
			}
		} else if rangeString[0] == '>' {
			_, err = fmt.Fscanf(strings.NewReader(rangeString[1:]), "%f", &from)

			if err == nil {
				to = clampTo
			}
		} else {
			_, _ = fmt.Fscanf(strings.NewReader(rangeString), "%f %f", &from, &to)
		}

		if from < clampFrom {
			from = clampFrom
		}

		if to > clampTo {
			to = clampTo
		}

		if from > to {
			if err != nil {
				fmt.Printf("Invalid range! %s\n", err.Error())
			} else {
				fmt.Println("Invalid range!")
			}
		}
	}

	return
}

func getBool(reader *bufio.Reader, message string, defaultValue bool) (value bool) {
	var defaultString = "y/N"

	if defaultValue {
		defaultString = "Y/n"
	}

	fmt.Printf("%s (%s) ", message, defaultString)
	stringValue, _ := getLine(reader)

	if len(stringValue) == 0 {
		return defaultValue
	}

	return stringValue[0] == 'y'
}

func printValuesAsTable(writer io.Writer, values []*data.Value) {
	var cells = make([][]string, len(values))

	for i, value := range values {
		cells[i] = []string{
			value.Name,
			strconv.Itoa(value.Count),
			fmt.Sprintf("%f", value.Value),
			fmt.Sprintf("%f", value.Total),
		}
	}

	utils.PrintTable(writer, &[]string{"Name", "Count", "Value", "Total"}, &cells)
}
