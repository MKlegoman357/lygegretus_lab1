package utils

import (
	"fmt"
	"io"
	"strconv"
	"strings"
)

func PrintTable(writer io.Writer, columnHeaders *[]string, cells *[][]string) {
	var columnWidths = make([]int, len(*columnHeaders))

	for i, header := range *columnHeaders {
		if len(header) > columnWidths[i] {
			columnWidths[i] = len(header)
		}
	}

	for _, row := range *cells {
		for i, cell := range row {
			if len(cell) > columnWidths[i] {
				columnWidths[i] = len(cell)
			}
		}
	}

	var separator = "+"

	_, _ = fmt.Fprint(writer, "|")
	for i, header := range *columnHeaders {
		_, _ = fmt.Fprintf(writer, " %-"+strconv.Itoa(columnWidths[i])+"s |", header)
		separator += strings.Repeat("-", columnWidths[i]+2) + "+"
	}

	_, _ = fmt.Fprintln(writer)
	_, _ = fmt.Fprintln(writer, separator)

	for _, row := range *cells {
		_, _ = fmt.Fprint(writer, "|")
		for i, cell := range row {
			_, _ = fmt.Fprintf(writer, " %"+strconv.Itoa(columnWidths[i])+"s |", cell)
		}
		_, _ = fmt.Fprintln(writer)
	}
}
