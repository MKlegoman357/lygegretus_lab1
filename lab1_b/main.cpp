#include <iostream>
#include <omp.h>
#include <iomanip>
#include <fstream>
#include "json.hpp"
#include "data/Value.h"
#include "data/SynchronizedBufferedValueQueue.h"
#include "data/SynchronizedBufferedOrderedValueQueue.h"

using namespace std;
using namespace data;
using json = nlohmann::json;

void createFilteredData(const string &inputFilePath, float totalFrom, float totalTo, bool showConsoleOutput,
                        const string &outputFilePath);

vector<Value *> *consumeAndFilterValues(
        vector<Value *> &values, float totalFrom, float totalTo, int workerCount, int bufferSize
);

vector<Value *> *consumeAndFilterValuesWithConsoleOutput(
        vector<Value *> &values, float totalFrom, float totalTo, int workerCount, int bufferSize
);

void printValuesAsTable(ostream &output, const vector<Value *> &values);

void printTable(ostream &output, int columns, int rows, string *columnHeaders, string *cells);

int main() {
    try {
        createFilteredData(
                "../IFF_7-11_Klimasauksas_M_L1_dat_1.json",
                0, 50, false,
                "../IFF_7-11_Klimasauksas_M_L1_rez_1.txt"
        );
        createFilteredData(
                "../IFF_7-11_Klimasauksas_M_L1_dat_2.json",
                0, 50, false,
                "../IFF_7-11_Klimasauksas_M_L1_rez_2.txt"
        );
        createFilteredData(
                "../IFF_7-11_Klimasauksas_M_L1_dat_3.json",
                0, 50, false,
                "../IFF_7-11_Klimasauksas_M_L1_rez_3.txt"
        );
    } catch (exception &e) {
        cout << e.what();
    }

    return 0;
}

void cleanupValues(vector<Value *> *values) {
    for (auto value : *values) { delete value; }
    delete values;
}

void createFilteredData(const string &inputFilePath, float totalFrom, float totalTo, bool showConsoleOutput,
                        const string &outputFilePath) {
    vector<Value *> *values;

    try {
        values = &readValuesFromFile(inputFilePath);
    } catch (exception &e) {
        cout << e.what() << endl;
        return;
    }

    int workerCount = omp_get_max_threads();
    int bufferSize = int(values->size()) / 2;

    vector<Value *> *filteredValues;

    if (showConsoleOutput) {
        filteredValues = consumeAndFilterValuesWithConsoleOutput(*values, totalFrom, totalTo, workerCount, bufferSize);
    } else {
        filteredValues = consumeAndFilterValues(*values, totalFrom, totalTo, workerCount, bufferSize);
    }

    if (outputFilePath.empty()) {
        cout << "Filtered values:" << endl;
        printValuesAsTable(cout, *filteredValues);
    } else {
        ofstream output(outputFilePath);

        if (!output) {
            cout << "An error occurred while trying to open the output file: " << outputFilePath << endl;
            cleanupValues(values);
            return;
        }

        output << "Original values:" << endl;
        printValuesAsTable(output, *values);

        output << endl << "Filtered values:" << endl;
        printValuesAsTable(output, *filteredValues);

        output.close();
    }

    cleanupValues(values);
}

vector<Value *> *consumeAndFilterValues(
        vector<Value *> &values, float totalFrom, float totalTo, int workerCount, int bufferSize
) {
    SynchronizedBufferedValueQueue inputQueue(bufferSize);
    SynchronizedBufferedOrderedValueQueue outputQueue(values.size());

#pragma omp parallel num_threads(workerCount) default(none) shared(values, totalFrom, totalTo, inputQueue, outputQueue)
    {
        if (omp_get_thread_num() == 0) {
            for (auto value : values) {
                inputQueue.Put(value);
            }

            inputQueue.Done();
        } else {
            auto takeValues = true;

            while (takeValues) {
                auto value = inputQueue.Take();

                if (value == nullptr) {
                    takeValues = false;
                } else {
                    value->total = float(value->count) * value->value;
                    for (auto i = 0; i < 2e6; i++) {} // do something useless

                    if (value->total >= totalFrom && value->total <= totalTo) {
                        outputQueue.Put(value);
                    }
                }
            }
        }
    }

    return &outputQueue.getValues();
}

vector<Value *> *consumeAndFilterValuesWithConsoleOutput(
        vector<Value *> &values, float totalFrom, float totalTo, int workerCount, int bufferSize
) {
    SynchronizedBufferedValueQueue inputQueue(bufferSize);
    SynchronizedBufferedOrderedValueQueue outputQueue(values.size());

    cout << "Creating workers: " << workerCount << endl;

#pragma omp parallel num_threads(workerCount) default(none) shared(values, totalFrom, totalTo, inputQueue, outputQueue, cout)
    {
        if (omp_get_thread_num() == 0) {
            for (auto value : values) {
#pragma omp critical (print_critical_section)
                {
                    cout << "Putting value: " << *value << endl;
                }
                inputQueue.Put(value);
            }

            inputQueue.Done();
        } else {
            auto takeValues = true;
            auto valuesProcessed = 0;

            while (takeValues) {
                auto value = inputQueue.Take();

                if (value == nullptr) {
#pragma omp critical (print_critical_section)
                    {
                        cout << omp_get_thread_num() << ". I'm done " << valuesProcessed << endl;
                    }
                    takeValues = false;
                } else {
                    valuesProcessed++;
                    value->total = float(value->count) * value->value;
                    for (auto i = 0; i < 2e6; i++) {} // do something useless

                    if (value->total >= totalFrom && value->total <= totalTo) {
#pragma omp critical (print_critical_section)
                        {
                            cout << omp_get_thread_num() << ". Putting value to output: " << *value << endl;
                        }
                        outputQueue.Put(value);
                    }
                }
            }
        }
    }

    auto &outputValues = outputQueue.getValues();

    cout << "Values:" << endl;
    print(outputValues);

    return &outputValues;
}

void printValuesAsTable(ostream &output, const vector<Value *> &values) {
    int columns = 4;
    int rows = values.size();

    string columnHeaders[] = {"Name", "Count", "Value", "Total"};
    string cells[columns * rows];

    for (int i = 0; i < values.size(); i++) {
        auto value = values[i];
        cells[i * columns + 0] = value->name;
        cells[i * columns + 1] = to_string(value->count);
        cells[i * columns + 2] = to_string(value->value);
        cells[i * columns + 3] = to_string(value->total);
    }

    printTable(output, columns, rows, columnHeaders, cells);
}

void printTable(ostream &output, int columns, int rows, string *columnHeaders, string *cells) {
    size_t i, j;
    int columnWidths[columns];

    for (i = 0; i < columns; i++) {
        columnWidths[i] = 0;

        if (columnHeaders[i].size() > columnWidths[i]) {
            columnWidths[i] = columnHeaders[i].size();
        }
    }

    for (j = 0; j < rows; j++) {
        for (i = 0; i < columns; i++) {
            if (cells[j * columns + i].size() > columnWidths[i]) {
                columnWidths[i] = cells[j * columns + i].size();
            }
        }
    }

    ostringstream separator;
    separator << "+";

    output << "|";
    for (i = 0; i < columns; i++) {
        output << " " << left << setw(columnWidths[i]) << columnHeaders[i] << " |";
        separator << string(columnWidths[i] + 2, '-') + "+";
    }

    output << endl << separator.str() << endl;

    for (j = 0; j < rows; j++) {
        output << "|";
        for (i = 0; i < columns; i++) {
            output << " " << right << setw(columnWidths[i]) << cells[j * columns + i] << " |";
        }
        output << endl;
    }
}