#ifndef LAB1_B_SYNCHRONIZEDBUFFEREDORDEREDVALUEQUEUE_H
#define LAB1_B_SYNCHRONIZEDBUFFEREDORDEREDVALUEQUEUE_H

#include "SynchronizedBufferedValueQueue.h"

namespace data {
    typedef int (compareFunc)(Value *a, Value *b);

    class SynchronizedBufferedOrderedValueQueue : SynchronizedBufferedValueQueue {
    protected:
        compareFunc *compare;

    public:
        explicit SynchronizedBufferedOrderedValueQueue(int count);

        SynchronizedBufferedOrderedValueQueue(int count, compareFunc *compare);

        void Put(Value *value) override;

        vector<Value *> &getValues();
    };
}

#endif //LAB1_B_SYNCHRONIZEDBUFFEREDORDEREDVALUEQUEUE_H
