#include <utility>
#include <vector>
#include <iostream>
#include <fstream>
#include "../json.hpp"
#include "Value.h"

using json = nlohmann::json;

using namespace std;

namespace data {

    Value::Value() = default;

    Value::Value(string name, int count, float value) :
            name(move(name)), count(count), value(value) {}

    string Value::toString() {
        ostringstream stringStream;
        stringStream << name << " (" << count << " * " << value << " = " << total << ")";
        return stringStream.str();
    }

    ostream &operator<<(ostream &os, Value &value) {
        os << value.toString();
        return os;
    }

    void to_json(json &j, const Value *&value) {
        j = json{
                {"name",  value->name},
                {"count", value->count},
                {"value", value->value}
        };
    }

    void from_json(const json &j, Value *&value) {
        value = new Value(
                j.at("name").get<string>(),
                j.at("count").get<int>(),
                j.at("value").get<float>()
        );
    }

    void print(vector<Value *> &values) {
        for (auto i = 0; i < values.size(); i++) {
            cout << i << ". " << *values[i] << endl;
        }
    }

    vector<Value *> &readValuesFromFile(const string &path) {
        json jsonData;

        ifstream input(path);

        if (!input) throw invalid_argument("The given path is not valid.");

        input >> jsonData;

        input.close();

        auto &values = *new vector<Value *>(jsonData.size());

        for (auto i = 0; i < jsonData.size(); i++) {
            jsonData[i].get_to<Value *>(values[i]);
        }

        return values;
    }
}