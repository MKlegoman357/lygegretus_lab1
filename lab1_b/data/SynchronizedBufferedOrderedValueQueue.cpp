#include "SynchronizedBufferedOrderedValueQueue.h"

namespace data {

    SynchronizedBufferedOrderedValueQueue::SynchronizedBufferedOrderedValueQueue(int count)
            : SynchronizedBufferedOrderedValueQueue(count, [](Value *a, Value *b) -> int {
        return a->total > b->total ? 1 : a->total < b->total ? -1 : 0;
    }) {}

    SynchronizedBufferedOrderedValueQueue::SynchronizedBufferedOrderedValueQueue(
            int count, compareFunc *compare
    ) : SynchronizedBufferedValueQueue(count), compare(compare) {}

    void SynchronizedBufferedOrderedValueQueue::Put(Value *value) {
        if (!AcquireWriteLock()) return;

        int index = count++;

        while (index > 0 && compare(values[index - 1], value) > 0) {
            values[index] = values[index - 1];
            index--;
        }

        values[index] = value;
        canWrite = count<bufferSize;
        canRead = count > 0;

        omp_unset_lock(lock);
    }

    vector<Value *> &SynchronizedBufferedOrderedValueQueue::getValues() {
        auto &list = *new vector<Value *>(count);

        for (auto i = 0; i < count; i++) {
            list[i] = values[i];
        }

        return list;
    }

}