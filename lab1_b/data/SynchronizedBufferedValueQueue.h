#ifndef LAB1_B_SYNCHRONIZEDBUFFEREDVALUEQUEUE_H
#define LAB1_B_SYNCHRONIZEDBUFFEREDVALUEQUEUE_H

#include <omp.h>
#include "Value.h"

namespace data {
    class SynchronizedBufferedValueQueue {
    protected:
        Value **values;
        int bufferSize;
        int count = 0;
        omp_lock_t *lock;
        bool done = false;

        bool canWrite = true;
        bool canRead = false;

    public:
        explicit SynchronizedBufferedValueQueue(int count);

        virtual ~SynchronizedBufferedValueQueue();

        virtual void Put(Value *value);

        virtual Value *Take();

        void Done();

    protected:
        bool AcquireWriteLock();

        bool AcquireReadLock();
    };
}

#endif //LAB1_B_SYNCHRONIZEDBUFFEREDVALUEQUEUE_H
