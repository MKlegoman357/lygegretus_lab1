#ifndef LAB1_B_VALUE_H
#define LAB1_B_VALUE_H

#include "../json.hpp"
#include <string>
#include <sstream>
#include <vector>

using namespace std;
using json = nlohmann::json;

namespace data {
    class Value {
    public:
        string name;
        int count = 0;
        float value = 0;
        float total = 0;

        Value();

        Value(string name, int count, float value);

        string toString();

        friend ostream &operator<<(ostream &os, Value &value);

        friend void to_json(json &j, const Value *&value);

        friend void from_json(const json &j, Value *&value);
    };

    void print(vector<Value *> &values);

    vector<Value *> &readValuesFromFile(const string &path);
}

#endif //LAB1_B_VALUE_H
