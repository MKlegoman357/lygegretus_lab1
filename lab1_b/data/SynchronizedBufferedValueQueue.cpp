#include <omp.h>
#include "SynchronizedBufferedValueQueue.h"

namespace data {

    SynchronizedBufferedValueQueue::SynchronizedBufferedValueQueue(int bufferSize) : bufferSize(bufferSize) {
        values = new Value *[bufferSize];
        lock = new omp_lock_t;
        omp_init_lock(lock);
    }

    SynchronizedBufferedValueQueue::~SynchronizedBufferedValueQueue() {
        omp_destroy_lock(lock);
        delete values;
        delete lock;
    }

    void SynchronizedBufferedValueQueue::Put(Value *value) {
        if (!AcquireWriteLock()) return;

        values[count++] = value;
        canWrite = count < bufferSize;
        canRead = count > 0;

        omp_unset_lock(lock);
    }

    Value *SynchronizedBufferedValueQueue::Take() {
        if (!AcquireReadLock()) return nullptr;

        Value *value = values[--count];
        canRead = count > 0;
        canWrite = count < bufferSize;

        omp_unset_lock(lock);

        return value;
    }

    bool SynchronizedBufferedValueQueue::AcquireWriteLock() {
        if (done) return false;

        omp_set_lock(lock);

        while (!canWrite && !done) {
            omp_unset_lock(lock);
            while (!canWrite && !done) {}
            if (done) return false;
            omp_set_lock(lock);
        }

        if (done) {
            omp_unset_lock(lock);
            return false;
        }

        return true;
    }

    bool SynchronizedBufferedValueQueue::AcquireReadLock() {
        if (done && !canRead) return false;

        omp_set_lock(lock);

        while (!canRead && !done) {
            omp_unset_lock(lock);
            while (!canRead && !done) {}
            if (done && !canRead) return false;
            omp_set_lock(lock);
        }

        if (done && !canRead) {
            omp_unset_lock(lock);
            return false;
        }

        return true;
    }

    void SynchronizedBufferedValueQueue::Done() {
        done = true;
    }

}